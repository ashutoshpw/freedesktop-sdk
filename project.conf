name: freedesktop-sdk

format-version: 14

aliases:
  (@): include/aliases.yml

(@): include/mirrors.yml

element-path: elements

fatal-warnings:
- overlaps

variables:
  (@):
  - include/arch.yml
  - include/build-dirs.yml
  - include/build-flags.yml
  - include/strip.yml
  - include/versions.yml

  branch: '%{freedesktop-sdk-flatpak-branch}'
  snap-branch: '%{freedesktop-sdk-snap-branch}'

  sysroot: /cross-installation
  tools: /cross

environment:
  (@): include/environment.yml

split-rules:
  (@): include/split-rules.yml

plugins:
  - origin: local
    path: plugins/sources
    sources:
      crate: 0

  - origin: local
    path: plugins/elements
    elements:
      check_forbidden: 0
      snap_image: 0
      export: 0
      re-import: 0

  - origin: pip
    package-name: buildstream-external
    elements:
      collect_integration: 0
      collect_manifest: 0
      flatpak_image: 0
      flatpak_repo: 0
      tar_element: 0
      x86image: 0
      oci: 0
    sources:
      git_tag: 1

options:
  (@): include/options.yml

artifacts:
  url: https://freedesktop-sdk-cache.codethink.co.uk:11001

elements:
  cmake:
    (@): include/cmake-conf.yml
  autotools:
    (@): include/autotools-conf.yml
  meson:
    (@): include/meson-conf.yml
  pip:
    (@): include/pip.yml
  distutils:
    (@): include/distutils.yml

sources:
  git_tag:
    (@): include/git_tag-conf.yml
